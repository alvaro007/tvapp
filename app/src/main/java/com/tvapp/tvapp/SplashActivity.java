package com.tvapp.tvapp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.tvapp.tvapp.backend.BackendPIONewUserEvent;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.model.UserLikes;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getName();

    private TVApplication application;

    private FunctionCallback<Void> launchMainActivityCallback =  new FunctionCallback<Void>() {
        @Override
        public void onFinish(Error error, Void data) {
            Intent intent = new Intent();
            intent.setClass(SplashActivity.this, MainActivity.class);

            SplashActivity.this.startActivity(intent);
            SplashActivity.this.finish();
        }
    };

    private FunctionCallback<Void> launchWelcomeActivityCallback = new FunctionCallback<Void>() {
        @Override
        public void onFinish(Error error, Void data) {
            Intent intent = new Intent();
            intent.setClass(SplashActivity.this, WelcomeActivity.class);

            SplashActivity.this.startActivityForResult(intent, 0);
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        this.setContentView(R.layout.splash);

        application = (TVApplication) SplashActivity.this.getApplication();

        signUpInParse();
    }

    private void signUpInParse() {

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            application.setUser(currentUser);
            createUserLikesParseObject(currentUser, launchMainActivityCallback);
        } else {

            final ParseUser user = new ParseUser();

            final String login = getGmailMainEmailAccount();
            final String password = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            user.setUsername(login);
            user.setPassword(password);

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e != null) {
                        Log.e(TAG, e.getMessage());

                        ParseUser.logInInBackground(login, password, new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {

                                if (e != null) {
                                    Log.e(TAG, e.getMessage());
                                } else {
                                    application.setUser(parseUser);
                                    createUserLikesParseObject(parseUser, launchMainActivityCallback);
                                }
                            }
                        });

                    } else {

                        sendPIONewUser(user.getUsername());

                        application.setUser(user);
                        createUserLikesParseObject(user, launchWelcomeActivityCallback);
                    }
                }
            });
        }
    }

    private void sendPIONewUser(String userId) {

        String pioAccessKey = getResources().getString(R.string.pio_application_access_key);

        TVAppBackend backend = new TVAppBackend();

        BackendPIONewUserEvent backendPIONewUserEvent = new BackendPIONewUserEvent(backend);
        backendPIONewUserEvent.sendNewUserEvent(pioAccessKey, userId, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.d(TAG, error.getMessage());
                }
            }
        });
    }

    private void createUserLikesParseObject(final ParseUser user, final FunctionCallback<Void> callback) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserLikes");
        query.whereEqualTo("user", user);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e != null || parseObjects.isEmpty()) {

                    ParseObject post = new ParseObject("UserLikes");
                    post.put("user", user);
                    post.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e != null) {
                                Log.e(TAG, e.getMessage());
                            } else {

                                callback.onFinish(null, null);
                            }
                        }
                    });

                    UserLikes userLikes = new UserLikes(post);
                    application.setUserLikes(userLikes);

                } else {

                    callback.onFinish(null, null);
                    UserLikes userLikes = new UserLikes(parseObjects.get(0));
                    application.setUserLikes(userLikes);
                }
            }
        });
    }

    private String getGmailMainEmailAccount() {

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(getApplicationContext()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }

        throw new RuntimeException("No email account found in device");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Intent intent = new Intent();
        intent.setClass(SplashActivity.this, MainActivity.class);

        SplashActivity.this.startActivity(intent);
        SplashActivity.this.finish();
    }
}
