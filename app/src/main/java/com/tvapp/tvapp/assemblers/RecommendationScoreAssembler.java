package com.tvapp.tvapp.assemblers;

import com.tvapp.tvapp.common.SimpleAssembler;
import com.tvapp.tvapp.dto.RecommendationScoreDTO;
import com.tvapp.tvapp.model.RecommendationScore;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class RecommendationScoreAssembler  extends SimpleAssembler<RecommendationScoreDTO, RecommendationScore> {


    @Override
    public RecommendationScore createDomainObject(RecommendationScoreDTO recommendationScoreDTO) {

        String item = recommendationScoreDTO.getItem();
        float score = recommendationScoreDTO.getScore();

        RecommendationScore recommendationScore = new RecommendationScore(item, score);

        return recommendationScore;
    }

    @Override
    public RecommendationScoreDTO createDataTransferObject(RecommendationScore recommendationScore) {

        RecommendationScoreDTO recommendationScoreDTO = new RecommendationScoreDTO();

        recommendationScoreDTO.setItem(recommendationScore.getItem());
        recommendationScoreDTO.setScore(recommendationScore.getScore());

        return recommendationScoreDTO;
    }
}
