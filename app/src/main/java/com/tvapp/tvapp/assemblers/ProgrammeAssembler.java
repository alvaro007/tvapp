package com.tvapp.tvapp.assemblers;

import com.tvapp.tvapp.common.SimpleAssembler;
import com.tvapp.tvapp.dto.ProgrammeDTO;
import com.tvapp.tvapp.model.Programme;

/**
 * Created by alvaro on 26/12/14.
 */
public class ProgrammeAssembler extends SimpleAssembler<ProgrammeDTO, Programme> {

    @Override
    public Programme createDomainObject(ProgrammeDTO programmeDTO) {

        Programme programme = new Programme();

        programme.setId(programmeDTO.get_id());
        programme.setStart(programmeDTO.getStart());
        programme.setStop(programmeDTO.getStop());
        programme.setTitle(programmeDTO.getTitle());
        programme.setDescription(programmeDTO.getDesc());
        programme.setChannel(programmeDTO.getChannel());
        programme.setCategory(programmeDTO.getCategory());
        programme.setImageUrl(programmeDTO.getImageUrl());
        programme.setImageTinyUrl(programmeDTO.getImageTinyUrl());
        programme.setSubTitle(programmeDTO.getSubTitle());

        return programme;
    }

    @Override
    public ProgrammeDTO createDataTransferObject(Programme domainObject) {
        return null;
    }
}
