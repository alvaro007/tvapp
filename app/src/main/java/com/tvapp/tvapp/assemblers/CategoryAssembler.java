package com.tvapp.tvapp.assemblers;

import com.tvapp.tvapp.common.SimpleAssembler;
import com.tvapp.tvapp.dto.CategoryDTO;
import com.tvapp.tvapp.model.Category;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class CategoryAssembler extends SimpleAssembler<CategoryDTO, Category> {

    @Override
    public Category createDomainObject(CategoryDTO categoryDTO) {
        
        String id = categoryDTO.getId();
        String name = categoryDTO.getName();

        return new Category(id, name);
    }

    @Override
    public CategoryDTO createDataTransferObject(Category category) {

        CategoryDTO categoryDTO = new CategoryDTO();

        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());

        return categoryDTO;
    }
}
