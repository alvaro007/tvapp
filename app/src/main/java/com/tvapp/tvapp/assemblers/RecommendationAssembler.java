package com.tvapp.tvapp.assemblers;

import com.tvapp.tvapp.common.SimpleAssembler;
import com.tvapp.tvapp.dto.RecommendationDTO;
import com.tvapp.tvapp.dto.RecommendationScoreDTO;
import com.tvapp.tvapp.model.Recommendation;
import com.tvapp.tvapp.model.RecommendationScore;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class RecommendationAssembler extends SimpleAssembler<RecommendationDTO, Recommendation> {


    private final RecommendationScoreAssembler recommendationScoreAssembler;

    public RecommendationAssembler(RecommendationScoreAssembler recommendationScoreAssembler) {
        this.recommendationScoreAssembler = recommendationScoreAssembler;
    }

    @Override
    public Recommendation createDomainObject(RecommendationDTO recommendationDTO) {

        boolean isOriginal = recommendationDTO.isOriginal();

        Collection<RecommendationScore> recommendationScores = new ArrayList<RecommendationScore>();

        for (RecommendationScoreDTO recommendationScoreDTO : recommendationDTO.getItemScores()) {

            RecommendationScore recommendationScore = recommendationScoreAssembler.createDomainObject(recommendationScoreDTO);
            recommendationScores.add(recommendationScore);
        }

        return new Recommendation(isOriginal, recommendationScores);
    }

    @Override
    public RecommendationDTO createDataTransferObject(Recommendation recommendation) {

        boolean isOriginal = recommendation.isOriginal();

        Collection<RecommendationScoreDTO> recommendationScoreDTOs = new ArrayList<RecommendationScoreDTO>();

        for (RecommendationScore recommendationScore : recommendation.getItemScores()) {

            RecommendationScoreDTO recommendationScoreDTO = recommendationScoreAssembler.createDataTransferObject(recommendationScore);
            recommendationScoreDTOs.add(recommendationScoreDTO);
        }

        RecommendationDTO recommendationDTO = new RecommendationDTO();
        recommendationDTO.setItemScores(recommendationScoreDTOs);
        recommendationDTO.setOriginal(isOriginal);

        return recommendationDTO;
    }
}
