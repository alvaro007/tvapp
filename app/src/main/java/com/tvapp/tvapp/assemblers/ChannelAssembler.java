package com.tvapp.tvapp.assemblers;

import com.tvapp.tvapp.common.SimpleAssembler;
import com.tvapp.tvapp.dto.ChannelDTO;
import com.tvapp.tvapp.model.Channel;

/**
 * Created by alvaro on 25/12/14.
 */
public class ChannelAssembler extends SimpleAssembler<ChannelDTO, Channel> {

    @Override
    public Channel createDomainObject(ChannelDTO channelDTO) {

        Channel channel = new Channel();
        channel.setId(channelDTO.get_id());
        channel.setNameId(channelDTO.getId());
        channel.setDisplayName(channelDTO.getDisplayName());
        channel.setImageUrl(channelDTO.getImageUrl());

        return channel;
    }

    @Override
    public ChannelDTO createDataTransferObject(Channel channel) {

        return null;
    }
}
