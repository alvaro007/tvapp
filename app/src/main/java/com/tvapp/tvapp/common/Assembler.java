package com.tvapp.tvapp.common;

import java.util.Collection;

/**
 * Interface that establishes the capacity of assemble objects of type T from K
 * and vice versa
 *
 * @author Alvaro.Arranz
 *
 * @param <T>
 *            data-transfer object type
 * @param <K>
 *            domain object type
 */
public interface Assembler<T, K> {

    /**
     * Creates a certain domain object from a data transfer object
     *
     * @param dataTransferObject
     * @return domain object
     */
    public K createDomainObject(T dataTransferObject);

    /**
     * Creates a data-transfer object from a domain object
     *
     * @param domainObject
     * @return data-transfer object
     */
    public T createDataTransferObject(K domainObject);

    /**
     * Transforms a group of data-transfer objects into domain objects
     *
     * @param collectionTimetableClassProfessorDTO
     * @return Collection of domain objects
     */
    public Collection<K> createDomainObjects(
            Collection<T> collectionTimetableClassProfessorDTO);

    /**
     * Transforms a group of domain objects into data-transfer objects
     *
     * @param collectionTimetableClassProfessor
     * @return Collection of data-transfer objects
     */
    public Collection<T> createDataTransferObjects(
            Collection<K> collectionTimetableClassProfessor);

}
