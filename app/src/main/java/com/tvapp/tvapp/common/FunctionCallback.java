package com.tvapp.tvapp.common;

/**
 * Created by alvaro on 25/12/14.
 */
public interface FunctionCallback<T> {

    public void onFinish(Error error, T data);
}
