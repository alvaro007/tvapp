package com.tvapp.tvapp.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

/**
 * Created by alvaro on 28/12/14.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private FunctionCallback<Bitmap> callback;

    public DownloadImageTask(FunctionCallback<Bitmap> callback) {
        this.callback = callback;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            callback.onFinish(null, result);
        } else {
            callback.onFinish(new Error("A problem occurred while downloading image "), null);
        }
    }
}
