package com.tvapp.tvapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.tvapp.tvapp.backend.BackendPIODeleteEvent;
import com.tvapp.tvapp.backend.BackendPIOViewEvent;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.common.MD5;
import com.tvapp.tvapp.model.UserLikes;
import com.tvapp.tvapp.ui.WelcomeCategoriesFragment;
import com.tvapp.tvapp.ui.WelcomeFinishFragment;
import com.tvapp.tvapp.ui.WelcomeMainFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class WelcomeActivity extends Activity {

    private static final String TAG = WelcomeActivity.class.getName();

    private Collection<String> categoriesSelected = new ArrayList<>();
    private Collection<String> categoriesNames = new ArrayList<>();

    private TVApplication application;

    private WelcomeMainFragment.WelcomeMainFragmentObserver welcomeMainFragmentObserver = new WelcomeMainFragment.WelcomeMainFragmentObserver() {
        @Override
        public void onContinueClick() {
            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);

            fragmentTransaction.replace(R.id.main_layout, welcomeCategoriesFragment, "mainFragment");

            fragmentTransaction.commit();
        }
    };

    private WelcomeCategoriesFragment.WelcomeCategoriesFragmentObserver welcomeCategoriesFragmentObserver = new WelcomeCategoriesFragment.WelcomeCategoriesFragmentObserver() {
        @Override
        public void onContinueClick(Collection<String> categoryNames, Collection<String> categoriesSelected) {

            WelcomeActivity.this.categoriesNames = categoryNames;
            WelcomeActivity.this.categoriesSelected = categoriesSelected;

            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);

            fragmentTransaction.replace(R.id.main_layout, welcomeFinishFragment, "finishFragment");

            fragmentTransaction.commit();
        }

        @Override
        public void onBackClick() {

            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);

            fragmentTransaction.replace(R.id.main_layout, welcomeMainFragment, "mainFragment");

            fragmentTransaction.commit();
        }
    };

    private WelcomeFinishFragment.WelcomeFinishFragmentObserver welcomeFinishFragmentObserver = new WelcomeFinishFragment.WelcomeFinishFragmentObserver() {
        @Override
        public void onContinueClick() {
            sendPIOSelectedCategories();
            WelcomeActivity.this.finish();
        }

        @Override
        public void onBackClick() {

            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);

            fragmentTransaction.replace(R.id.main_layout, welcomeCategoriesFragment, "categoriesFragment");

            fragmentTransaction.commit();
        }
    };

    private WelcomeMainFragment welcomeMainFragment;
    private WelcomeCategoriesFragment welcomeCategoriesFragment;
    private WelcomeFinishFragment welcomeFinishFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.welcome_activity_main);

        application = (TVApplication) getApplication();

        FragmentManager fragmentManager = getFragmentManager();

        welcomeMainFragment = (WelcomeMainFragment) fragmentManager.findFragmentByTag("mainFragment");
        welcomeCategoriesFragment = (WelcomeCategoriesFragment) fragmentManager.findFragmentByTag("categoriesFragment");
        welcomeFinishFragment = (WelcomeFinishFragment) fragmentManager.findFragmentByTag("finishFragment");

        if (welcomeMainFragment == null) {
            welcomeMainFragment = new WelcomeMainFragment();
        }

        if (welcomeCategoriesFragment == null) {
            welcomeCategoriesFragment = new WelcomeCategoriesFragment();
        }

        if (welcomeFinishFragment == null) {
            welcomeFinishFragment = new WelcomeFinishFragment();
        }

        if (savedInstanceState == null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_layout, welcomeMainFragment, "mainFragment");

            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        welcomeMainFragment.addObserver(welcomeMainFragmentObserver);
        welcomeCategoriesFragment.addObserver(welcomeCategoriesFragmentObserver);
        welcomeFinishFragment.addObserver(welcomeFinishFragmentObserver);
    }

    @Override
    protected void onPause() {
        super.onPause();

        welcomeMainFragment.removeObserver(welcomeMainFragmentObserver);
        welcomeCategoriesFragment.removeObserver(welcomeCategoriesFragmentObserver);
        welcomeFinishFragment.removeObserver(welcomeFinishFragmentObserver);
    }

    @Override
    public void onBackPressed() {
    }

    private void sendPIOSelectedCategories() {

        UserLikes userLikes = application.getUserLikes();

        Set<String> categoriesNamesSet = new TreeSet<String>();
        categoriesNamesSet.addAll(categoriesNames);

        Set<String> selectedCategoriesSet = new TreeSet<String>();
        selectedCategoriesSet.addAll(categoriesSelected);

        Collection<String> categoriesToRemove = new ArrayList<String>();

        // Remove events associated with removed categories
        for (String category : userLikes.getProgrammes()) {

            // check if it is not a film
            if (categoriesNamesSet.contains(category) && !selectedCategoriesSet.contains(category)) {
                removePIOCategory(category);
                categoriesToRemove.add(category);
            }
        }

        for (String category : categoriesToRemove) {
            userLikes.removeProgramme(category);
        }

        // Add events for new categories
        for (String category : selectedCategoriesSet) {

            if (!userLikes.containsProgramme(category)) {
                sendPIOCategory(category);
                userLikes.addProgramme(category);
            }
        }

    }

    private void removePIOCategory(String category) {

        TVAppBackend backend = new TVAppBackend();

        String user = application.getUser().getUsername();
        String pioKey = getResources().getString(R.string.pio_application_access_key);
        String itemId = MD5.md5(category);

        BackendPIODeleteEvent backendPIODeleteEvent = new BackendPIODeleteEvent(backend);
        backendPIODeleteEvent.removeEvent(pioKey, user, itemId, new FunctionCallback<Void>() {
            @Override
            public void onFinish(Error error, Void data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                }
            }
        });
    }

    private void sendPIOCategory(String category) {

        TVAppBackend backend = new TVAppBackend();

        String user = application.getUser().getUsername();
        String pioKey = getResources().getString(R.string.pio_application_access_key);
        String itemId = MD5.md5(category);

        BackendPIOViewEvent backendPIOViewEvent = new BackendPIOViewEvent(backend);
        backendPIOViewEvent.sendViewEvent(pioKey, user, itemId, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                }
            }
        });

    }

}
