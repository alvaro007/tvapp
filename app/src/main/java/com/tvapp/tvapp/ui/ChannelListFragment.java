package com.tvapp.tvapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.adapters.ChannelAdapter;
import com.tvapp.tvapp.backend.Backend;
import com.tvapp.tvapp.backend.BackendChannelProvider;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.model.Channel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alvaro on 30/12/14.
 */
public class ChannelListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ChannelListFragment.class.getName();

    private ListView listView;
    private BaseAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) rootView.findViewById(R.id.main_list);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Backend backend = new TVAppBackend();

        BackendChannelProvider provider = new BackendChannelProvider();
        provider.getChannels(backend, new FunctionCallback<Collection<Channel>>() {
            @Override
            public void onFinish(Error error, Collection<Channel> data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {
                    List<Channel> listChannels = new ArrayList<Channel>();
                    listChannels.addAll(data);
                    adapter = new ChannelAdapter(ChannelListFragment.this
                            .getActivity(), listChannels);

                    listView.setAdapter(adapter);

                }
            }
        });
    }

    @Override
    public void onRefresh() {
        adapter.notifyDataSetChanged();
    }
}
