package com.tvapp.tvapp.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.WelcomeActivity;

import java.util.ArrayList;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class WelcomeMainFragment extends Fragment {

    private static final String TAG = WelcomeActivity.class.getName();

    private Button continueButton;
    private WelcomeMainFragmentObservers observers = new WelcomeMainFragmentObservers();

    public void addObserver(WelcomeMainFragmentObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(WelcomeMainFragmentObserver observer) {
        observers.remove(observer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.welcome_main, container, false);

        continueButton = (Button) rootView.findViewById(R.id.welcome_continue);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observers.onContinueClick();
            }
        });

        return rootView;
    }

    public interface WelcomeMainFragmentObserver {

        public void onContinueClick();
    }

    public class WelcomeMainFragmentObservers extends ArrayList<WelcomeMainFragmentObserver>
            implements WelcomeMainFragmentObserver {

        @Override
        public void onContinueClick() {
            for (WelcomeMainFragmentObserver observer : this) {
                observer.onContinueClick();
            }
        }
    }
}
