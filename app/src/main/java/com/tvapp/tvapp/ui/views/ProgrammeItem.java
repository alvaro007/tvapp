package com.tvapp.tvapp.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.controllers.api.ProgrammeItemController;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.provider.BitmapProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class ProgrammeItem {

    private static final String TAG = ProgrammeItem.class.getName();

    private View view;
    private Context context;
    private Programme programme;
    private BitmapProvider bitmapProvider;
    private ProgrammeItemController controller;

    public View getView() {
        return view;
    }

    public Programme getProgramme() {
        return programme;
    }

    public ProgrammeItem(Context context, BitmapProvider bitmapProvider, ProgrammeItemController controller) {

        this.context = context;
        this.bitmapProvider = bitmapProvider;
        this.controller = controller;
    }

    public void initItem(final Programme programme) {

        this.programme = programme;

        LayoutInflater inflater = LayoutInflater.from(context);

        view = inflater.inflate(R.layout.programme_item, null);

        if (programme.getImageUrl() != null && !programme.getImageUrl().equals("")) {
            final ImageView imageView = (ImageView) view.findViewById(R.id.image);

            bitmapProvider.getBitmap(programme.getImageTinyUrl(), new FunctionCallback<Bitmap>() {
                @Override
                public void onFinish(Error error, Bitmap data) {
                    if (error != null) {
                        Log.e(TAG, error.getMessage());
                    } else {
                        imageView.setImageBitmap(data);
                    }
                }
            });
        }

        TextView textViewTitle = (TextView) view.findViewById(R.id.title);
        textViewTitle.setText(programme.getTitle());

        TextView textViewDescription = (TextView) view.findViewById(R.id.channel);
        textViewDescription.setText(programme.getChannel());

        final ImageView imageViewLike = (ImageView) view.findViewById(R.id.like_button);
        imageViewLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.onLikeClick(programme.getTitle());

                if (controller.userLikesProgramme(programme.getTitle())) {
                    imageViewLike.setImageDrawable(ProgrammeItem.this.context.getResources().getDrawable(R.drawable.check_ok));
                } else {
                    imageViewLike.setImageDrawable(ProgrammeItem.this.context.getResources().getDrawable(R.drawable.check_alt));
                }
            }
        });

        if (controller.userLikesProgramme(programme.getTitle())) {
            imageViewLike.setImageDrawable(ProgrammeItem.this.context.getResources().getDrawable(R.drawable.check_ok));
        }

        TextView textSubTitle = (TextView) view.findViewById(R.id.subtitle);
        if (programme.getSubTitle() != null) {
            textSubTitle.setText(programme.getSubTitle());
        } else {
            textSubTitle.setVisibility(View.GONE);
        }

        Date date = Calendar.getInstance().getTime();
        long timeNow = date.getTime();

        String dateFormat = "yyyyMMddHHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        try {
            Date start = sdf.parse(programme.getStart());
            Date stop = sdf.parse(programme.getStop());

            long timeStart = start.getTime();
            long timeStop = stop.getTime();

            int timeTotal = (int) (timeStop - timeStart);

            int progress = (int) ((((timeNow-timeStart)*1.0f)/(timeTotal*1.0f)) * 100);

            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setProgress(progress);

            String hourFormat = "HH:mm";
            SimpleDateFormat hourDateFormat = new SimpleDateFormat(hourFormat);
            String hourStart = hourDateFormat.format(start);
            String hourStop = hourDateFormat.format(stop);

            TextView textStart = (TextView) view.findViewById(R.id.start);
            TextView textStop = (TextView) view.findViewById(R.id.stop);

            textStart.setText(hourStart);
            textStop.setText(hourStop);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
