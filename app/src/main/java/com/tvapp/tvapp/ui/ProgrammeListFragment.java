package com.tvapp.tvapp.ui;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tvapp.tvapp.ProgrammeActivity;
import com.tvapp.tvapp.R;
import com.tvapp.tvapp.TVApplication;
import com.tvapp.tvapp.adapters.ProgrammeAdapter;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.controllers.api.ProgrammeItemController;
import com.tvapp.tvapp.controllers.impl.ProgrammeItemControllerImpl;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.UserLikes;
import com.tvapp.tvapp.recommender.api.RecommenderEventSender;
import com.tvapp.tvapp.recommender.impl.RecommenderEventSenderImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alvaro on 30/12/14.
 */
public class ProgrammeListFragment extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ProgrammeListFragment.class.getName();
    private static final String PROGRAMME_SERIALIZABLE_ID = "PROGRAMME";

    private ListView listView;
    private ProgrammeAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgrammeItemController programmeItemController;
    private TVApplication application;

    private ProgrammeListFragmentObservers observers = new ProgrammeListFragmentObservers();

    public void addObserver(ProgrammeListFragmentObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ProgrammeListFragmentObserver observer) {
        observers.remove(observer);
    }

    private FunctionCallback<Collection<Programme>> updateListener = new FunctionCallback<Collection<Programme>>() {
        @Override
        public void onFinish(Error error, Collection<Programme> data) {

            if (error != null) {
                Log.e(TAG, error.getMessage());
            } else {
                List<Programme> listProgrammes = new ArrayList<Programme>();
                listProgrammes.addAll(data);

                adapter.setProgrammes(listProgrammes);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) rootView.findViewById(R.id.main_list);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);

        application = (TVApplication) getActivity().getApplication();

        RecommenderEventSender eventSender = new RecommenderEventSenderImpl(this.getActivity(), application.getUser().getUsername());

        programmeItemController = new ProgrammeItemControllerImpl(application.getUserLikes(), eventSender);
        adapter = new ProgrammeAdapter(ProgrammeListFragment.this.getActivity(),
                application.getImageCache(), programmeItemController, new ArrayList<Programme>());

        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                swipeRefreshLayout.setRefreshing(false);
            }
        });

        listView.setOnItemClickListener(ProgrammeListFragment.this);
        listView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        observers.onUpdateProgrammes(updateListener);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Log.d(TAG, "Click on item " + i);

        Programme programme = (Programme) adapter.getItem(i);

        Log.d(TAG, "Item: " + programme.getTitle());

        Intent intent = new Intent(this.getActivity(), ProgrammeActivity.class);
        intent.putExtra(PROGRAMME_SERIALIZABLE_ID, programme);

        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        observers.onUpdateProgrammes(updateListener);
    }

    public interface ProgrammeListFragmentObserver {

        public void onUpdateProgrammes(FunctionCallback<Collection<Programme>> callback);
    }

    public class ProgrammeListFragmentObservers extends ArrayList<ProgrammeListFragmentObserver>
            implements ProgrammeListFragmentObserver {

        @Override
        public void onUpdateProgrammes(FunctionCallback<Collection<Programme>> callback) {
            for (ProgrammeListFragmentObserver observer : this) {
                observer.onUpdateProgrammes(callback);
            }
        }
    }

    /*
    private void updateProgrammes() {

        Backend backend = new TVAppBackend();

        BackendProgrammeNowProvider provider = new BackendProgrammeNowProvider();
        provider.getProgrammes(backend, new FunctionCallback<Collection<Programme>>() {

            @Override
            public void onFinish(Error error, Collection<Programme> data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {
                    List<Programme> listProgrammes = new ArrayList<Programme>();
                    listProgrammes.addAll(data);

                    adapter.setProgrammes(listProgrammes);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void getRecommendations(List<Programme> programmes) {

        TVAppBackend backend = new TVAppBackend();

        String user = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        Collection<String> showIds = new ArrayList<>();

        for (Programme programme : programmes) {
            showIds.add(MD5.md5(programme.getTitle()));
        }

        BackendPIORecommendationProvider provider = new BackendPIORecommendationProvider(backend);
        provider.getRecommendation(user, showIds, new FunctionCallback<Recommendation>() {
            @Override
            public void onFinish(Error error, Recommendation data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {
                     for (RecommendationScore recommendationScore : data.getItemScores()) {
                        Log.i(TAG, "Item: " + recommendationScore.getItem() + " score: " + recommendationScore.getScore());
                    }
                }
            }
        });
    }
    */


}
