package com.tvapp.tvapp.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.TVApplication;
import com.tvapp.tvapp.backend.BackendCategoriesProvider;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.model.Category;
import com.tvapp.tvapp.model.UserLikes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class WelcomeCategoriesFragment extends Fragment {

    private static final String TAG = WelcomeCategoriesFragment.class.getName();

    private Button continueButton;
    private Button backButton;
    private ListView listView;
    private ArrayAdapter<String> adapter;

    private String[] categoryNames;
    private List<String> selectedCategories = new ArrayList<>();

    private WelcomeCategoriesFragmentObservers observers = new WelcomeCategoriesFragmentObservers();

    public void addObserver(WelcomeCategoriesFragmentObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(WelcomeCategoriesFragmentObserver observer) {
        observers.remove(observer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.welcome_categories, container, false);

        continueButton = (Button) rootView.findViewById(R.id.welcome_continue);
        backButton = (Button) rootView.findViewById(R.id.welcome_back);

        listView = (ListView) rootView.findViewById(R.id.welcome_category_list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        loadCategoriesAsync();

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();

                selectedCategories.clear();

                for (int i = 0; i < sparseBooleanArray.size(); i++) {
                    if (sparseBooleanArray.get(i)) {
                        selectedCategories.add(categoryNames[i]);
                    }
                }

                observers.onContinueClick(Arrays.asList(categoryNames), selectedCategories);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observers.onBackClick();
            }
        });

        TVApplication application = (TVApplication) getActivity().getApplication();
        UserLikes userLikes = application.getUserLikes();

        selectedCategories.clear();
        selectedCategories.addAll(userLikes.getProgrammes());

        return rootView;
    }

    private void loadCategoriesAsync() {

        TVAppBackend backend = new TVAppBackend();
        BackendCategoriesProvider provider = new BackendCategoriesProvider();
        provider.getCategories(backend, new FunctionCallback<Collection<Category>>() {
            @Override
            public void onFinish(Error error, Collection<Category> data) {

                categoryNames = new String[data.size()];

                int count = 0;
                for (Category category : data) {
                    categoryNames[count] = category.getName();
                    count++;
                }

                Arrays.sort(categoryNames);

                adapter = new ArrayAdapter<String>(WelcomeCategoriesFragment.this.getActivity(),
                        android.R.layout.simple_list_item_multiple_choice, categoryNames);
                listView.setAdapter(adapter);

                checkCategories();
            }
        });
    }

    private void checkCategories() {

        Set<String> selectedCategoriesSet = new TreeSet<String>();
        selectedCategoriesSet.addAll(selectedCategories);

        for (int i = 0; i < categoryNames.length; i++) {
            String categoryName = categoryNames[i];

            if (selectedCategoriesSet.contains(categoryName)) {
                listView.setItemChecked(i, true);
            } else {
                listView.setItemChecked(i, false);
            }
        }
    }

    public interface WelcomeCategoriesFragmentObserver {

        void onContinueClick(Collection<String> allCategories, Collection<String> categoriesSelected);
        void onBackClick();
    }

    public class WelcomeCategoriesFragmentObservers extends ArrayList<WelcomeCategoriesFragment.WelcomeCategoriesFragmentObserver>
            implements WelcomeCategoriesFragmentObserver {

        @Override
        public void onContinueClick(Collection<String> allCategories, Collection<String> categoriesSelected) {
            for (WelcomeCategoriesFragmentObserver observer : this) {
                observer.onContinueClick(allCategories, categoriesSelected);
            }
        }

        @Override
        public void onBackClick() {
            for (WelcomeCategoriesFragmentObserver observer : this) {
                observer.onBackClick();
            }
        }
    }

}

