package com.tvapp.tvapp.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tvapp.tvapp.R;

import java.util.ArrayList;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class WelcomeFinishFragment extends Fragment {

    private Button continueButton;
    private Button backButton;
    private WelcomeFinishFragmentObservers observers = new WelcomeFinishFragmentObservers();

    public void addObserver(WelcomeFinishFragmentObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(WelcomeFinishFragmentObserver observer) {
        observers.remove(observer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.welcome_finish, container, false);

        continueButton = (Button) rootView.findViewById(R.id.welcome_continue);
        backButton = (Button) rootView.findViewById(R.id.welcome_back);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observers.onContinueClick();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observers.onBackClick();
            }
        });

        return rootView;
    }

    public interface WelcomeFinishFragmentObserver {

        public void onContinueClick();
        public void onBackClick();
    }

    public class WelcomeFinishFragmentObservers extends ArrayList<WelcomeFinishFragmentObserver>
            implements WelcomeFinishFragmentObserver {

        @Override
        public void onContinueClick() {
            for (WelcomeFinishFragmentObserver observer : this) {
                observer.onContinueClick();
            }
        }

        @Override
        public void onBackClick() {
            for (WelcomeFinishFragmentObserver observer : this) {
                observer.onBackClick();
            }
        }
    }
}
