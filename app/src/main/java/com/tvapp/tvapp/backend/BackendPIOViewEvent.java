package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.RecommendationViewEventDTO;

/**
 * Created by alvaroarranz on 22/04/15.
 */
public class BackendPIOViewEvent {

    private static String TAG = BackendPIOViewEvent.class.getName();

    private final Backend backend;

    public BackendPIOViewEvent(Backend backend) {
        this.backend = backend;
    }

    public void sendViewEvent(String pioAppAccessKey, String userId, String itemId, final FunctionCallback<String> callback) {

        String url =  backend.getPIORecomenderEventsUrl() + "?accessKey=" + pioAppAccessKey;

        RequestConfiguration requestConfiguration = new RequestConfiguration(backend.getRecommendationEventsHost(), url, "POST");
        requestConfiguration.addHeader("Content-Type", "application/json");

        RecommendationViewEventDTO recommendationViewEventDTO = new RecommendationViewEventDTO(userId, itemId);

        Gson gson = new Gson();
        requestConfiguration.setBody(gson.toJson(recommendationViewEventDTO));

        BackendCaller backendCaller = new BackendCaller();

        backendCaller.request(requestConfiguration, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);
                } else {
                    callback.onFinish(null, "");
                }
            }
        });
    }

}
