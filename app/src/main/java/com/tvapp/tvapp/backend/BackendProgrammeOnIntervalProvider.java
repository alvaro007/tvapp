package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tvapp.tvapp.assemblers.ProgrammeAssembler;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.ProgrammeDTO;
import com.tvapp.tvapp.model.Programme;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Created by alvaroarranz on 05/05/15.
 */
public class BackendProgrammeOnIntervalProvider {

    private static final String TAG = BackendProgrammeOnIntervalProvider.class.getName();

    private Backend backend;
    private ProgrammeAssembler assembler = new ProgrammeAssembler();

    public BackendProgrammeOnIntervalProvider(Backend backend) {
        this.backend = backend;
    }

    public void getProgrammesOnInterval(Date dateFrom, Date dateTo, final FunctionCallback<Collection<Programme>> callback) {

        String dateFormat = "yyyyMMddHHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        String dateFromString = sdf.format(dateFrom);
        String dateToString = sdf.format(dateTo);

        String url = backend.getProgrammesOnInterval() + "?startTime=" + dateFromString + "&endTime=" + dateToString;
        RequestConfiguration config = new RequestConfiguration(backend.getTVServicesHost(), url, "GET");

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(config, new FunctionCallback<String>() {

            @Override
            public void onFinish(Error error, String data) {

                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);
                } else {
                    Gson gson = new Gson();

                    Type collectionType = new TypeToken<Collection<ProgrammeDTO>>(){}
                            .getType();
                    Collection<ProgrammeDTO> programmeDTOs = gson.fromJson(data,
                            collectionType);

                    Collection<Programme> programmes = assembler.createDomainObjects(programmeDTOs);

                    callback.onFinish(null, programmes);
                }
            }
        });
    }
}
