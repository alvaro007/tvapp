package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.RecommendationSetEventDTO;

/**
 * Created by alvaroarranz on 05/05/15.
 */
public class BackendPIONewUserEvent {

    private static final String TAG = BackendPIONewUserEvent.class.getName();

    private Backend backend;

    public Backend getBackend() {
        return backend;
    }

    public BackendPIONewUserEvent(Backend backend) {
        this.backend = backend;
    }

    public void sendNewUserEvent(String pioAppAccessKey, String userId, final FunctionCallback<String> callback) {

        String url =  backend.getPIORecomenderEventsUrl() + "?accessKey=" + pioAppAccessKey;

        RequestConfiguration requestConfiguration = new RequestConfiguration(backend.getRecommendationEventsHost(), url, "POST");
        requestConfiguration.addHeader("Content-Type", "application/json");

        RecommendationSetEventDTO recommendationSetEventDTO = new RecommendationSetEventDTO(userId);

        Gson gson = new Gson();
        requestConfiguration.setBody(gson.toJson(recommendationSetEventDTO));

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(requestConfiguration, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);
                } else {
                    callback.onFinish(null, "");
                }
            }
        });
    }

}
