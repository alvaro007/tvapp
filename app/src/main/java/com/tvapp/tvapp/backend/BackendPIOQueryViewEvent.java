package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.PIO.EventDTO;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class BackendPIOQueryViewEvent {

    private static final String TAG = BackendPIOQueryViewEvent.class.getName();

    private Backend backend;

    public Backend getBackend() {
        return backend;
    }

    public BackendPIOQueryViewEvent(Backend backend) {
        this.backend = backend;
    }

    public void sendViewEventQuery(String pioAppAccessKey, String userId,
                                   String itemId, final FunctionCallback<Collection<EventDTO>> callback) {

        String url =  backend.getPIORecomenderEventsUrl() + "?accessKey=" + pioAppAccessKey;
        url += "&entityType=user";
        url += "&entityId=" + userId;
        url += "&event=view";
        url += "&targetEntityId=" + itemId;

        RequestConfiguration requestConfiguration = new RequestConfiguration(backend.getRecommendationEventsHost(), url, "GET");
        requestConfiguration.addHeader("Content-Type", "application/json");

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(requestConfiguration, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);

                } else {
                    Gson gson = new Gson();

                    Type collectionType = new TypeToken<Collection<EventDTO>>(){}
                            .getType();
                    Collection<EventDTO> eventDTOs = gson.fromJson(data, collectionType);

                    callback.onFinish(null, eventDTOs);
                }
            }
        });
    }
}
