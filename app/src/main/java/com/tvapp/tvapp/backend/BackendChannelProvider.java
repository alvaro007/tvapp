package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tvapp.tvapp.assemblers.ChannelAssembler;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.ChannelDTO;
import com.tvapp.tvapp.model.Channel;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by alvaro on 25/12/14.
 */
public class BackendChannelProvider {

    private static final String TAG = BackendChannelProvider.class.getName();

    private ChannelAssembler assembler = new ChannelAssembler();

    public void getChannels(Backend backend, final FunctionCallback<Collection<Channel>> callback) {

        RequestConfiguration config = new RequestConfiguration(backend.getTVServicesHost(), backend.getChannelsUrl(), "GET");

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(config,
                new FunctionCallback<String>() {

                    @Override
                    public void onFinish(Error error, String data) {
                        if (error != null) {
                            Log.e(TAG, error.getMessage());
                            callback.onFinish(error, null);
                        } else {
                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<Collection<ChannelDTO>>(){}
                                    .getType();
                            Collection<ChannelDTO> channelsDTO = gson.fromJson(data,
                                    collectionType);

                            Collection<Channel> channels = assembler.createDomainObjects
                                    (channelsDTO);

                            callback.onFinish(null, channels);
                        }
                    }
                });
    }
}
