package com.tvapp.tvapp.backend;

/**
 * Created by alvaro on 25/12/14.
 */
public class TVAppBackend implements Backend {

    @Override
    public String getTVServicesHost() {
        return "http://upteev.com/";
    }

    @Override
    public String getChannelsUrl() {
        return "tvapp/channels";
    }

    @Override
    public String getProgrammesUrl() {
        return "tvapp/programmes";
    }

    @Override
    public String getProgrammesNowUrl() {
        return "tvapp/programmesNow";
    }

    @Override
    public String getCategoriesUrl() {
        return "tvapp/categories";
    }

    @Override
    public String getProgrammesOnInterval() {
        return "tvapp/programmesOnInterval";
    }

    @Override
    public String getRecommendationHost() {
        return "http://upteev.com:8000/";
    }

    @Override
    public String getPIORecommenderUrl() {
        return "queries.json";
    }

    @Override
    public String getRecommendationEventsHost() {
        return "http://upteev.com:8001/";
    }

    @Override
    public String getPIORecomenderEventsUrl() {
        return "events.json";
    }

    @Override
    public String getPIORecommenderEventsIdUrl() {
        return "events/";
    }

}
