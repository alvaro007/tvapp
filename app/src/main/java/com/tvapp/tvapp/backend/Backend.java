package com.tvapp.tvapp.backend;

/**
 * Created by alvaro on 25/12/14.
 */
public interface Backend {

    public String getTVServicesHost();

    public String getChannelsUrl();

    public String getProgrammesUrl();

    public String getProgrammesNowUrl();

    public String getCategoriesUrl();

    public String getProgrammesOnInterval();

    public String getRecommendationHost();

    public String getPIORecommenderUrl();

    public String getRecommendationEventsHost();

    public String getPIORecomenderEventsUrl();

    public String getPIORecommenderEventsIdUrl();
}
