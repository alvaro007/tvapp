package com.tvapp.tvapp.backend;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created by alvaro on 25/12/14.
 */
public class RequestConfiguration {

    private String host;
    private String url;
    private String method;
    private String body;
    private Collection<Map.Entry<String, String> > headers;

    /**
     *
     * @param method
     */
    public RequestConfiguration(String host, String url, String method) {
        this.host = host;
        this.method = method;
        this.url = url;

        body = null;
        headers = new ArrayList<Map.Entry<String, String> >();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }
    /**
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     *
     * @return
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @return
     */
    public Collection<Map.Entry<String, String> > getHeaders() {
        return headers;
    }

    /**
     *
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     *
     * @param key
     * @param value
     */
    public void addHeader(String key, String value) {

        headers.add(new AbstractMap.SimpleEntry<String, String>(key, value));
    }

    @Override
    public String toString() {
        return method + " " + host + url + " " + body;
    }

}
