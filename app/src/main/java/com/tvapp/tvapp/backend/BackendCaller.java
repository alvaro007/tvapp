package com.tvapp.tvapp.backend;

import android.os.AsyncTask;
import android.util.Log;

import com.tvapp.tvapp.common.FunctionCallback;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

/**
 * Created by alvaro on 25/12/14.
 */
public class BackendCaller {

    private static final String TAG = BackendCaller.class.getName();

    private class MyAsyncReturn {

        private Error error;
        private int responseCode;
        private String returnMsg;

        public MyAsyncReturn(Error error) {
            this.error = error;
        }

        public MyAsyncReturn(int responseCode, String returnMsg) {
            this.returnMsg = returnMsg;
            this.responseCode = responseCode;
        }

        public Error getError() {
            return error;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getReturnMsg() {
            return returnMsg;
        }
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, MyAsyncReturn> {

        private RequestConfiguration req;
        private FunctionCallback<String> callback;

        /**
         *
         * @param req
         * @param callback
         */
        public MyAsyncTask(RequestConfiguration req,
                           FunctionCallback<String> callback) {
            this.req = req;
            this.callback = callback;
        }

        @Override
        protected MyAsyncReturn doInBackground(Void... voids) {
            HttpURLConnection connection = null;
            MyAsyncReturn asyncReturn = null;

            Log.d(TAG, req.toString());

            try {
                URL url = new URL(req.getHost() + req.getUrl());
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(req.getMethod());

                for (Map.Entry<String, String> header : req.getHeaders()) {
                    connection.setRequestProperty(header.getKey(), header.getValue());
                }

                if (req.getMethod().equals("GET") || req.getMethod().equals("DELETE") ) {
                    connection.connect();
                } else {
                    if (req.getBody() != null) {
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.write(req.getBody().getBytes());
                        wr.close();
                        wr.flush();
                    } else {
                        Log.e(TAG, "Body was not set");
                    }
                }

                String decodedString;
                String returnMsg = "";
                BufferedReader in = new BufferedReader(new InputStreamReader(connection
                        .getInputStream()));
                while ((decodedString = in.readLine()) != null) {
                    returnMsg+=decodedString;
                }
                in.close();
                connection.disconnect();
                asyncReturn = new MyAsyncReturn(connection.getResponseCode(), returnMsg);

                Log.d(TAG, "response: " + returnMsg);

            } catch (MalformedURLException e) {
                e.printStackTrace();
                asyncReturn = new MyAsyncReturn(new Error(e.getMessage()));
            } catch (ProtocolException e) {
                e.printStackTrace();
                asyncReturn = new MyAsyncReturn(new Error(e.getMessage()));
            } catch (IOException e) {
                e.printStackTrace();
                asyncReturn = new MyAsyncReturn(new Error(e.getMessage()));
            }

            return asyncReturn;
        }

        @Override
        protected void onPostExecute(MyAsyncReturn result){
            callback.onFinish(result.getError(), result.getReturnMsg());
        }
    }

    public void request(RequestConfiguration req,
                         FunctionCallback<String> callback) {

        MyAsyncTask task = new MyAsyncTask(req, callback);
        task.execute();
    }

}
