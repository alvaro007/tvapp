package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tvapp.tvapp.assemblers.CategoryAssembler;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.CategoryDTO;
import com.tvapp.tvapp.model.Category;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class BackendCategoriesProvider {

    private static final String TAG = BackendCategoriesProvider.class.getName();

    private CategoryAssembler assembler = new CategoryAssembler();

    public void getCategories(Backend backend, final FunctionCallback<Collection<Category>> callback) {

        RequestConfiguration config = new RequestConfiguration(backend.getTVServicesHost(), backend.getCategoriesUrl(), "GET");

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(config, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);
                } else {
                    Gson gson = new Gson();
                    Type collectionType = new TypeToken<Collection<CategoryDTO>>(){}
                            .getType();

                    Collection<CategoryDTO> categriesDTO = gson.fromJson(data,
                            collectionType);

                    Collection<Category> categories = assembler.createDomainObjects
                            (categriesDTO);

                    callback.onFinish(null, categories);
                }
            }
        });

    }
}
