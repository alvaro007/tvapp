package com.tvapp.tvapp.backend;

import android.util.Log;

import com.google.gson.Gson;
import com.tvapp.tvapp.assemblers.RecommendationAssembler;
import com.tvapp.tvapp.assemblers.RecommendationScoreAssembler;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.RecommendationDTO;
import com.tvapp.tvapp.dto.RecommendationRequest;
import com.tvapp.tvapp.model.Recommendation;

import java.util.Collection;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class BackendPIORecommendationProvider {

    private static String TAG = BackendPIORecommendationProvider.class.getName();

    private final Backend backend;
    private final RecommendationAssembler recommendationAssembler;

    public BackendPIORecommendationProvider(Backend backend) {
        this.backend = backend;

        RecommendationScoreAssembler recommendationScoreAssembler = new RecommendationScoreAssembler();
        this.recommendationAssembler = new RecommendationAssembler(recommendationScoreAssembler);
    }

    public void getRecommendation(String userId, Collection<String> showIds, final FunctionCallback<Recommendation> callback) {

        RequestConfiguration requestConfiguration = new RequestConfiguration(backend.getRecommendationHost(), backend.getPIORecommenderUrl(), "POST");
        requestConfiguration.addHeader("Content-Type", "application/json");

        RecommendationRequest recommendationRequest = new RecommendationRequest(userId, showIds);

        Gson gson = new Gson();
        requestConfiguration.setBody(gson.toJson(recommendationRequest));

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(requestConfiguration, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);
                } else {

                    Gson gson = new Gson();
                    RecommendationDTO recommendationDTO = gson.fromJson(data, RecommendationDTO.class);
                    Recommendation recommendation = recommendationAssembler.createDomainObject(recommendationDTO);

                    callback.onFinish(null, recommendation);
                }
            }
        });
    }
}
