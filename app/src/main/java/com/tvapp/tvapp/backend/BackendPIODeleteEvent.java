package com.tvapp.tvapp.backend;

import android.util.Log;

import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.dto.PIO.EventDTO;

import java.util.Collection;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class BackendPIODeleteEvent {

    private static final String TAG = BackendPIODeleteEvent.class.getName();

    private Backend backend;

    public BackendPIODeleteEvent(Backend backend) {
        this.backend = backend;
    }

    public void removeEvent(String pioAppAccessKey, String eventId, final FunctionCallback<String> callback) {

        String url =  backend.getPIORecommenderEventsIdUrl() + eventId + ".json?accessKey=" + pioAppAccessKey;

        RequestConfiguration requestConfiguration = new RequestConfiguration(backend.getRecommendationEventsHost(), url, "DELETE");
        requestConfiguration.addHeader("Content-Type", "application/json");

        BackendCaller backendCaller = new BackendCaller();
        backendCaller.request(requestConfiguration, new FunctionCallback<String>() {

            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                    callback.onFinish(error, null);

                } else {
                    callback.onFinish(null, "");
                }
            }
        });
    }

    public void removeEvent(final String pioAppAccessKey, String userId, String itemId, final FunctionCallback<Void> callback) {

        BackendPIOQueryViewEvent backendPIOQueryViewEvent = new BackendPIOQueryViewEvent(backend);
        final BackendPIODeleteEvent backendPIODeleteEvent = new BackendPIODeleteEvent(backend);

        backendPIOQueryViewEvent.sendViewEventQuery(pioAppAccessKey, userId, itemId, new FunctionCallback<Collection<EventDTO>>() {
            @Override
            public void onFinish(Error error, Collection<EventDTO> data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {

                    for (EventDTO eventDTO : data) {
                        backendPIODeleteEvent.removeEvent(pioAppAccessKey, eventDTO.getEventId(), new FunctionCallback<String>() {
                            @Override
                            public void onFinish(Error error, String data) {
                                if (error != null) {
                                    Log.e(TAG, error.getMessage());
                                } else {
                                    callback.onFinish(null, null);
                                }
                            }
                        });
                    }
                }
            }
        });

    }
}
