package com.tvapp.tvapp.model;

import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.tvapp.tvapp.common.MD5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import bolts.Task;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class UserLikes {

    private static final String TAG = UserLikes.class.getName();

    private ParseObject parseObject;
    private HashSet<String> filmsSet;

    public UserLikes(ParseObject parseObject) {
        this.parseObject = parseObject;

        filmsSet = new HashSet<>();
        if (parseObject.containsKey("films")) {

            Object returned = parseObject.get("films");
            if (returned instanceof ArrayList) {
                filmsSet.addAll((ArrayList<String>) parseObject.get("films"));
            }
        }
    }

    public Collection<String> getProgrammes() {
        return filmsSet;
    }

    public void addProgramme(String programmeName) {
        filmsSet.add(programmeName);
    }

    public void removeProgramme(String programmeName) {
        filmsSet.remove(programmeName);
    }

    public boolean containsProgramme(String programmeName) {
        return filmsSet.contains(programmeName);
    }

    public void saveInBackgroud() {

        parseObject.put("films", new ArrayList<String>(filmsSet));

        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    public String toString() {

        String out = "";

        for (String key : parseObject.keySet()) {

            out += key + ", ";
        }

        return out;
    }

}
