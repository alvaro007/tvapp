package com.tvapp.tvapp.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by alvaro on 25/12/14.
 */
public class Programme implements Serializable {

    private String id;
    private String start;
    private String stop;
    private String title;
    private String channel;
    private String description;
    private Collection<String> category;
    private String imageUrl;
    private String subTitle;

    private String imageTinyUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<String> getCategory() {
        return category;
    }

    public void setCategory(Collection<String> category) {
        this.category = category;
    }

    public String getImageTinyUrl() {
        return imageTinyUrl;
    }

    public void setImageTinyUrl(String imageTinyUrl) {
        this.imageTinyUrl = imageTinyUrl;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

}
