package com.tvapp.tvapp.model;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class RecommendationScore implements Comparable<RecommendationScore> {

    private final String item;
    private final float score;

    public RecommendationScore(String item, float score) {
        this.item = item;
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    public String getItem() {
        return item;
    }

    @Override
    public int compareTo(RecommendationScore another) {
        return - Float.compare(score, another.score);
    }
}
