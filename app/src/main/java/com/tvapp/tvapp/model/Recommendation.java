package com.tvapp.tvapp.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class Recommendation {

    private List<RecommendationScore> itemScores;
    private final boolean isOriginal;

    public Recommendation(boolean isOriginal, Collection<RecommendationScore> scores) {
        this.isOriginal = isOriginal;

        this.itemScores = new ArrayList<>();
        this.itemScores.addAll(scores);

        Collections.sort(itemScores);
    }

    public Collection<RecommendationScore> getItemScores() {
        return itemScores;
    }

    public boolean isOriginal() {
        return isOriginal;
    }
}
