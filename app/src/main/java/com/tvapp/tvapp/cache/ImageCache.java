package com.tvapp.tvapp.cache;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.tvapp.tvapp.common.DownloadImageTask;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.provider.BitmapProvider;

/**
 * Created by alvaro on 9/01/15.
 */
public class ImageCache implements BitmapProvider {

    LruCache<String, Bitmap> bitmapCache;

    public ImageCache(int cacheSize) {

        /*
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        bitmapCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
        */

        bitmapCache = new LruCache<String, Bitmap>(cacheSize);

    }

    public void getBitmap(final String url, final FunctionCallback<Bitmap> callback) {

        if (bitmapCache.get(url) != null) {
            callback.onFinish(null, bitmapCache.get(url));
        } else {
            DownloadImageTask task = new DownloadImageTask(new FunctionCallback<Bitmap>() {
                @Override
                public void onFinish(Error error, Bitmap data) {
                    if (error != null) {
                        callback.onFinish(error, null);
                    } else {
                        bitmapCache.put(url, data);
                        callback.onFinish(null, data);
                    }
                }
            });
            task.execute(url);
        }
    }

}
