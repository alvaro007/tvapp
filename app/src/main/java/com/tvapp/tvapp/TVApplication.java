package com.tvapp.tvapp;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.tvapp.tvapp.cache.ImageCache;
import com.tvapp.tvapp.model.UserLikes;
import com.tvapp.tvapp.provider.BitmapProvider;

/**
 * Created by alvaro on 9/01/15.
 */
public class TVApplication extends Application {

    private BitmapProvider bitmapProvider;

    private UserLikes userLikes;
    private ParseUser user;

    public void setUser(ParseUser parseUser) {
        user = parseUser;
    }

    public ParseUser getUser() {
        return user;
    }

    public void setUserLikes(UserLikes userLikes) {
        this.userLikes = userLikes;
    }

    public UserLikes getUserLikes() {
        return userLikes;
    }

    public void onCreate() {
        super.onCreate();

        bitmapProvider = new ImageCache(100);

        // Parse initialization
        ParseCrashReporting.enable(this);
        Parse.initialize(this, getResources().getString(R.string.parse_application_id), getResources().getString(R.string.parse_client_key));
    }

    public BitmapProvider getImageCache() {
        return bitmapProvider;
    }

}
