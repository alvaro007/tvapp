package com.tvapp.tvapp.provider;

import android.graphics.Bitmap;

import com.tvapp.tvapp.common.FunctionCallback;

/**
 * Created by alvaro on 9/01/15.
 */
public interface BitmapProvider {

    public void getBitmap(final String url, final FunctionCallback<Bitmap> callback);
}
