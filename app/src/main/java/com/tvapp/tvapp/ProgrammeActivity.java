package com.tvapp.tvapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseAnalytics;
import com.tvapp.tvapp.adapters.CategoryAdapter;
import com.tvapp.tvapp.backend.BackendPIOViewEvent;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.common.MD5;
import com.tvapp.tvapp.model.Programme;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alvaro on 30/12/14.
 */
public class ProgrammeActivity extends ActionBarActivity {

    private static final String TAG = ProgrammeActivity.class.getName();

    private static final String PROGRAMME_SERIALIZABLE_ID = "PROGRAMME";

    private ImageView imageView;
    private TextView startView;
    private TextView stopView;
    private ProgressBar progressBar;
    private TextView descriptionView;
    private TextView titleView;
    private ListView categoryList;
    private CategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.programme_main);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        imageView = (ImageView) findViewById(R.id.image);
        startView = (TextView) findViewById(R.id.start);
        stopView = (TextView) findViewById(R.id.stop);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        descriptionView = (TextView) findViewById(R.id.description);
        titleView = (TextView) findViewById(R.id.title);
        categoryList = (ListView) findViewById(R.id.categories);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().containsKey(PROGRAMME_SERIALIZABLE_ID)) {
                Programme programme = (Programme) intent.getExtras().getSerializable
                        (PROGRAMME_SERIALIZABLE_ID);
                updateProgramme(programme);

                sendParseStatistics(programme);
            }
        }
    }

    private void sendParseStatistics(Programme programme) {

        /*
        for (String category : programme.getCategory()) {
            Map<String, String> data = new HashMap<>();
            data.put("category", category);
            ParseAnalytics.trackEventInBackground("view_category", data);
        }
        */

        Map<String, String> data = new HashMap<>();
        data.put("programme", programme.getTitle());
        data.put("channel", programme.getChannel());
        ParseAnalytics.trackEventInBackground("view_program", data);
    }

    private void updateProgramme(Programme programme) {

        TVApplication application = (TVApplication) getApplication();

        if (programme.getImageUrl() != null && !programme.getImageUrl().equals("")) {

            application.getImageCache().getBitmap(programme.getImageUrl(), new FunctionCallback<Bitmap>() {
                @Override
                public void onFinish(Error error, Bitmap data) {
                    imageView.setImageBitmap(data);
                }
            });
        }

        List<String> categories = new ArrayList<String>();
        categories.addAll(programme.getCategory());

        categoryAdapter = new CategoryAdapter(this, R.layout.category_item, categories);
        categoryList.setAdapter(categoryAdapter);

        descriptionView.setText(programme.getDescription());
        titleView.setText(programme.getTitle());

        Date date = Calendar.getInstance().getTime();
        long timeNow = date.getTime();

        String dateFormat = "yyyyMMddHHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        try {
            Date start = sdf.parse(programme.getStart());
            Date stop = sdf.parse(programme.getStop());

            long timeStart = start.getTime();
            long timeStop = stop.getTime();

            int timeTotal = (int) (timeStop - timeStart);

            int progress = (int) ((((timeNow-timeStart)*1.0f)/(timeTotal*1.0f)) * 100);

            progressBar.setProgress(progress);

            String hourFormat = "HH:mm";
            SimpleDateFormat hourDateFormat = new SimpleDateFormat(hourFormat);
            String hourStart = hourDateFormat.format(start);
            String hourStop = hourDateFormat.format(stop);

            startView.setText(hourStart);
            stopView.setText(hourStop);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
