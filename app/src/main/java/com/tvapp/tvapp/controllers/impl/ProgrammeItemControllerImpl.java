package com.tvapp.tvapp.controllers.impl;

import com.tvapp.tvapp.controllers.api.ProgrammeItemController;
import com.tvapp.tvapp.model.UserLikes;
import com.tvapp.tvapp.recommender.api.RecommenderEventSender;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class ProgrammeItemControllerImpl implements ProgrammeItemController {

    private UserLikes userLikes;
    private RecommenderEventSender recommenderEventSender;

    public ProgrammeItemControllerImpl(UserLikes userLikes, RecommenderEventSender recommenderEventSender) {
        this.userLikes = userLikes;
        this.recommenderEventSender = recommenderEventSender;
    }

    @Override
    public boolean userLikesProgramme(String programmeName) {
        return userLikes.containsProgramme(programmeName);
    }

    @Override
    public void onLikeClick(String programmeName) {

        if (userLikesProgramme(programmeName)) {
            userLikes.removeProgramme(programmeName);
            recommenderEventSender.removeViewEvent(programmeName);
        } else {
            userLikes.addProgramme(programmeName);
            recommenderEventSender.sendViewEvent(programmeName);
        }
    }
}
