package com.tvapp.tvapp.controllers.api;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public interface ProgrammeItemController {

    public boolean userLikesProgramme(String programmeName);

    public void onLikeClick(String programmeName);
}
