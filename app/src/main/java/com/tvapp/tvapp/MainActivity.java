package com.tvapp.tvapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseAnalytics;
import com.tvapp.tvapp.backend.Backend;
import com.tvapp.tvapp.backend.BackendPIORecommendationProvider;
import com.tvapp.tvapp.backend.BackendProgrammeNowProvider;
import com.tvapp.tvapp.backend.BackendProgrammeOnIntervalProvider;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.common.MD5;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.Recommendation;
import com.tvapp.tvapp.model.RecommendationScore;
import com.tvapp.tvapp.recommender.api.RecommenderFilter;
import com.tvapp.tvapp.recommender.impl.RecommenderFilterImpl;
import com.tvapp.tvapp.ui.ProgrammeListFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = MainActivity.class.getName();

    private RecommenderFilter recommenderFilter = new RecommenderFilterImpl();

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private LinearLayout mDrawerLinearLayout;

    private TVApplication application;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private ProgrammeListFragment.ProgrammeListFragmentObserver nowFragmentObserver = new ProgrammeListFragment.ProgrammeListFragmentObserver() {
        @Override
        public void onUpdateProgrammes(final FunctionCallback<Collection<Programme>> callback) {
            Backend backend = new TVAppBackend();

            BackendProgrammeNowProvider provider = new BackendProgrammeNowProvider();
            provider.getProgrammes(backend, new FunctionCallback<Collection<Programme>>() {

                @Override
                public void onFinish(Error error, final Collection<Programme> programmes) {
                    if (error != null) {
                        Log.e(TAG, error.getMessage());
                        callback.onFinish(error, null);

                    } else {

                        getRecommendations(programmes, new FunctionCallback<Recommendation>() {
                            @Override
                            public void onFinish(Error error, Recommendation recommendation) {
                                if (error != null) {
                                    Log.e(TAG, error.getMessage());
                                } else {

                                    for (RecommendationScore recommendationScore : recommendation.getItemScores()) {
                                        Log.i(TAG, "Item: " + recommendationScore.getItem() + " score: " + recommendationScore.getScore());
                                    }

                                    List<Programme> recommendedProgrammes = recommenderFilter.filter(programmes, recommendation, -10f);
                                    callback.onFinish(null, recommendedProgrammes);
                                }
                            }
                        });

                        //callback.onFinish(null, programmes);

                    }
                }
            });
        }
    };

    private ProgrammeListFragment.ProgrammeListFragmentObserver todayFragmentObserver = new ProgrammeListFragment.ProgrammeListFragmentObserver() {
        @Override
        public void onUpdateProgrammes(final FunctionCallback<Collection<Programme>> callback) {

            Date dateNow = Calendar.getInstance().getTime();

            Calendar cal = Calendar.getInstance();

            cal.setTime(dateNow);
            cal.add(Calendar.DAY_OF_YEAR, 1);
            cal.set(Calendar.HOUR, 3);

            Date dateTo = cal.getTime();

            Backend backend = new TVAppBackend();
            BackendProgrammeOnIntervalProvider backendProgrammeOnIntervalProvider = new BackendProgrammeOnIntervalProvider(backend);
            backendProgrammeOnIntervalProvider.getProgrammesOnInterval(dateNow, dateTo, new FunctionCallback<Collection<Programme>>() {
                @Override
                public void onFinish(Error error, final Collection<Programme> programmes) {
                    if (error != null) {
                        Log.e(TAG, error.getMessage());
                        callback.onFinish(error, null);

                    } else {

                        getRecommendations(programmes, new FunctionCallback<Recommendation>() {
                            @Override
                            public void onFinish(Error error, Recommendation recommendation) {
                                if (error != null) {
                                    Log.e(TAG, error.getMessage());
                                    callback.onFinish(error, null);
                                } else {

                                    for (RecommendationScore recommendationScore : recommendation.getItemScores()) {
                                        Log.i(TAG, "Item: " + recommendationScore.getItem() + " score: " + recommendationScore.getScore());
                                    }

                                    List<Programme> recommendedProgrammes = recommenderFilter.filter(programmes, recommendation, -10f);
                                    callback.onFinish(null, recommendedProgrammes);
                                }
                            }
                        });

                        //callback.onFinish(null, programmes);
                    }
                }
            });

        }
    };

    private ProgrammeListFragment.ProgrammeListFragmentObserver soonFragmentObserver = new ProgrammeListFragment.ProgrammeListFragmentObserver() {
        @Override
        public void onUpdateProgrammes(FunctionCallback<Collection<Programme>> callback) {
            callback.onFinish(null, new ArrayList<Programme>());
        }
    };

    private void getRecommendations(Collection<Programme> programmes, FunctionCallback<Recommendation> callback) {

        TVAppBackend backend = new TVAppBackend();

        TVApplication application = (TVApplication) getApplication();

        String user = application.getUser().getUsername();
        Set<String> showIds = new TreeSet<>();

        for (Programme programme : programmes) {
            showIds.add(MD5.md5(programme.getTitle()));
        }

        BackendPIORecommendationProvider provider = new BackendPIORecommendationProvider(backend);
        provider.getRecommendation(user, showIds, callback);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Parse analytics
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        application = (TVApplication) getApplication();

        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinearLayout = (LinearLayout) findViewById(R.id.left_drawer);

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        final ActionBar actionBar = getSupportActionBar();
        // Specify that tabs should be displayed in the action bar.
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                actionBar.setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                actionBar.setTitle(mDrawerTitle);
            }
        };

        setDrawerLayoutWidth();

        TextView userTextView = (TextView) findViewById(R.id.user);
        userTextView.setText(application.getUser().getUsername());
    }

    private void setDrawerLayoutWidth() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        int newWidth = (int) (width * 0.9);

        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerLinearLayout.getLayoutParams();
        params.width = newWidth;
        mDrawerLinearLayout.setLayoutParams(params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_preferences) {

            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();

        TVApplication application = (TVApplication) getApplication();
        application.getUserLikes().saveInBackgroud();
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

            fragments = new ArrayList<Fragment>();

            ProgrammeListFragment nowFragment = new ProgrammeListFragment();
            nowFragment.addObserver(nowFragmentObserver);

            ProgrammeListFragment todayFragment = new ProgrammeListFragment();
            todayFragment.addObserver(todayFragmentObserver);

            ProgrammeListFragment soonFragment = new ProgrammeListFragment();
            soonFragment.addObserver(soonFragmentObserver);

            fragments.add(nowFragment);
            fragments.add(todayFragment);
            //fragments.add(soonFragment);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();

            switch (position) {
                case 0:
                    return getString(R.string.title_now).toUpperCase(l);
                case 1:
                    return getString(R.string.title_today).toUpperCase(l);
                case 2:
                    return getString(R.string.title_soon).toUpperCase(l);
            }
            return null;
        }
    }



}
