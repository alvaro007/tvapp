package com.tvapp.tvapp.dto;

import java.util.Collection;

/**
 * Created by alvaroarranz on 22/04/15.
 */
public class RecommendationRequest {

    private String user;
    private Collection<String> items;

    public RecommendationRequest(String user, Collection<String> items) {
        this.user = user;
        this.items = items;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Collection<String> getItems() {
        return items;
    }

    public void setItems(Collection<String> items) {
        this.items = items;
    }

}
