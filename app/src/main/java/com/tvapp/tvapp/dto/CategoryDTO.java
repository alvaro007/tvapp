package com.tvapp.tvapp.dto;

/**
 * Created by alvaroarranz on 23/04/15.
 */
public class CategoryDTO {

    private String _id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }
}
