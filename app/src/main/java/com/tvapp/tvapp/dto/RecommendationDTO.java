package com.tvapp.tvapp.dto;

import java.util.Collection;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class RecommendationDTO {

    private Collection<RecommendationScoreDTO> itemScores;
    private boolean isOriginal;

    public Collection<RecommendationScoreDTO> getItemScores() {
        return itemScores;
    }

    public void setItemScores(Collection<RecommendationScoreDTO> itemScores) {
        this.itemScores = itemScores;
    }

    public boolean isOriginal() {
        return isOriginal;
    }

    public void setOriginal(boolean isOriginal) {
        this.isOriginal = isOriginal;
    }
}
