package com.tvapp.tvapp.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alvaro on 25/12/14.
 */
public class ChannelDTO {

    private String _id;
    private String id;
    private String displayName;
    @SerializedName("image")
    private String imageUrl;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
