package com.tvapp.tvapp.dto;

import com.google.gson.annotations.SerializedName;

import java.util.Collection;

/**
 * Created by alvaro on 26/12/14.
 */
public class ProgrammeDTO {

    private String _id;
    private String start;
    private String stop;
    private String title;
    private String channel;
    private String desc;
    private Collection<String> category;
    private String subTitle;

    @SerializedName("image")
    private String imageUrl;

    @SerializedName("image_tiny")
    private String imageTinyUrl;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Collection<String> getCategory() {
        return category;
    }

    public void setCategory(Collection<String> category) {
        this.category = category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageTinyUrl() {
        return imageTinyUrl;
    }

    public void setImageTinyUrl(String imageTinyUrl) {
        this.imageTinyUrl = imageTinyUrl;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
