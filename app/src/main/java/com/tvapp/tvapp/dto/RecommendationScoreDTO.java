package com.tvapp.tvapp.dto;

/**
 * Created by alvaroarranz on 21/04/15.
 */
public class RecommendationScoreDTO {

    private String item;
    private float score;

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
