package com.tvapp.tvapp.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tvapp.tvapp.controllers.api.ProgrammeItemController;
import com.tvapp.tvapp.controllers.impl.ProgrammeItemControllerImpl;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.UserLikes;
import com.tvapp.tvapp.provider.BitmapProvider;
import com.tvapp.tvapp.ui.views.ProgrammeItem;

import java.util.List;

/**
 * Created by alvaro on 26/12/14.
 */
public class ProgrammeAdapter extends BaseAdapter {

    private static final String TAG = ProgrammeAdapter.class.getName();

    private Context context;
    private List<Programme> programmes;
    private BitmapProvider bitmapProvider;
    private ProgrammeItemController programmeItemController;

    public ProgrammeAdapter(Context context, BitmapProvider bitmapProvider, ProgrammeItemController programmeItemController, List<Programme> programmes) {
        this.context = context;
        this.programmes = programmes;
        this.bitmapProvider = bitmapProvider;

        this.programmeItemController = programmeItemController;
    }

    public void setProgrammes(List<Programme> programmes) {
        this.programmes = programmes;
    }

    @Override
    public int getCount() {
        return programmes.size();
    }

    @Override
    public Object getItem(int i) {
        return programmes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        final Programme programme = (Programme) getItem(i);

        ProgrammeItem programmeItem = new ProgrammeItem(context, bitmapProvider, programmeItemController);
        programmeItem.initItem(programme);

        return programmeItem.getView();
    }

}
