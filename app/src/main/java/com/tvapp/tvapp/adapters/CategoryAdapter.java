package com.tvapp.tvapp.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by alvaro on 9/01/15.
 */
public class CategoryAdapter extends ArrayAdapter<String> {

    public CategoryAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }
}
