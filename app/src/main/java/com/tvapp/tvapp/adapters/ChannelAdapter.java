package com.tvapp.tvapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.common.DownloadImageTask;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.model.Channel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alvaro on 26/12/14.
 */
public class ChannelAdapter extends BaseAdapter {

    private static final String TAG = ChannelAdapter.class.getName();

    private Context context;
    private List<Channel> channels;
    private Map<String, Bitmap> bitmaps = new HashMap<String, Bitmap>();

    public ChannelAdapter(Context context, List<Channel> objects) {
        this.context = context;
        this.channels = objects;
    }

    @Override
    public int getCount() {
        return channels.size();
    }

    @Override
    public Object getItem(int i) {
        return channels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Channel channel = (Channel) getItem(position);
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.channel_item, null);

        final ImageView imageView = (ImageView) view.findViewById(R.id.image);

        if (bitmaps.containsKey(channel.getId())) {
            imageView.setImageBitmap(bitmaps.get(channel.getId()));
        } else {
            DownloadImageTask task = new DownloadImageTask(new FunctionCallback<Bitmap>() {
                @Override
                public void onFinish(Error error, Bitmap data) {
                    if (error != null) {
                        Log.e(TAG, error.getMessage());
                    } else {
                        imageView.setImageBitmap(data);
                        bitmaps.put(channel.getId(), data);
                    }
                }
            });
            task.execute(channel.getImageUrl());
        }

        TextView textViewName = (TextView) view.findViewById(R.id.name);
        textViewName.setText(channel.getNameId());

        return view;
    }


}
