package com.tvapp.tvapp.recommender.api;

import com.tvapp.tvapp.model.Programme;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public interface RecommenderEventSender {

    public void sendViewEvent(String programeName);

    public void removeViewEvent(String programeName);
}
