package com.tvapp.tvapp.recommender.api;

import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.Recommendation;

import java.util.Collection;
import java.util.List;

/**
 * Created by alvaroarranz on 05/05/15.
 */
public interface RecommenderFilter {

    public List<Programme> filter(Collection<Programme> programmes, Recommendation recommendation, float threshold);

}
