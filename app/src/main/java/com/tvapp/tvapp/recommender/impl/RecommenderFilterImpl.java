package com.tvapp.tvapp.recommender.impl;

import com.tvapp.tvapp.common.MD5;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.Recommendation;
import com.tvapp.tvapp.model.RecommendationScore;
import com.tvapp.tvapp.recommender.api.RecommenderFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by alvaroarranz on 05/05/15.
 */
public class RecommenderFilterImpl implements RecommenderFilter {

    @Override
    public List<Programme> filter(Collection<Programme> programmes, Recommendation recommendation, float threshold) {

        Map<String, Collection<Programme>> programmeMap = new HashMap<>();

        for (Programme programme : programmes) {
            String programmeHashed = MD5.md5(programme.getTitle());

            if (!programmeMap.containsKey(programmeHashed)) {
                programmeMap.put(programmeHashed, new LinkedHashSet<Programme>());
            }

            programmeMap.get(MD5.md5(programme.getTitle())).add(programme);
        }

        List<Programme> filteredProgrammes = new ArrayList<>();

        for (RecommendationScore recommendationScore : recommendation.getItemScores()) {
            if (recommendationScore.getScore() > threshold) {
                Collection<Programme> programmeList = programmeMap.get(recommendationScore.getItem());
                filteredProgrammes.addAll(programmeList);
            }
        }

        return filteredProgrammes;
    }
}
