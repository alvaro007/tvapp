package com.tvapp.tvapp.recommender.impl;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.tvapp.tvapp.R;
import com.tvapp.tvapp.backend.BackendPIODeleteEvent;
import com.tvapp.tvapp.backend.BackendPIOQueryViewEvent;
import com.tvapp.tvapp.backend.BackendPIOViewEvent;
import com.tvapp.tvapp.backend.TVAppBackend;
import com.tvapp.tvapp.common.FunctionCallback;
import com.tvapp.tvapp.common.MD5;
import com.tvapp.tvapp.dto.PIO.EventDTO;
import com.tvapp.tvapp.model.Programme;
import com.tvapp.tvapp.model.Recommendation;
import com.tvapp.tvapp.recommender.api.RecommenderEventSender;

import java.util.Collection;

/**
 * Created by alvaroarranz on 04/05/15.
 */
public class RecommenderEventSenderImpl implements RecommenderEventSender {

    private static final String TAG = RecommenderEventSenderImpl.class.getName();

    private Context context;
    private String user;

    public RecommenderEventSenderImpl(Context context, String user) {
        this.context = context;
        this.user = user;
    }

    @Override
    public void sendViewEvent(String programmeName) {

        Log.i(TAG, "Sending View Event for programme " + programmeName);

        TVAppBackend backend = new TVAppBackend();

        final String itemId = MD5.md5(programmeName);

        String pioAppAccessKey = context.getResources().getString(R.string.pio_application_access_key);

        BackendPIOViewEvent backendPIOViewEvent = new BackendPIOViewEvent(backend);
        backendPIOViewEvent.sendViewEvent(pioAppAccessKey, user, itemId, new FunctionCallback<String>() {
            @Override
            public void onFinish(Error error, String data) {
                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {
                    Log.i(TAG, "View event succeeded, itemId: " + itemId + " userId: " + user + " data: " + data);
                }
            }
        });
    }

    @Override
    public void removeViewEvent(String programmeName) {

        Log.i(TAG, "Removing View Event for programme " + programmeName);

        TVAppBackend backend = new TVAppBackend();

        final String itemId = MD5.md5(programmeName);

        final String pioAppAccessKey = context.getResources().getString(R.string.pio_application_access_key);

        final BackendPIODeleteEvent backendPIODeleteEvent = new BackendPIODeleteEvent(backend);

        BackendPIOQueryViewEvent backendPIOQueryViewEvent = new BackendPIOQueryViewEvent(backend);
        backendPIOQueryViewEvent.sendViewEventQuery(pioAppAccessKey, user, itemId, new FunctionCallback<Collection<EventDTO>>() {

            @Override
            public void onFinish(Error error, Collection<EventDTO> data) {

                if (error != null) {
                    Log.e(TAG, error.getMessage());
                } else {

                    for (EventDTO eventDTO : data) {
                        backendPIODeleteEvent.removeEvent(pioAppAccessKey, eventDTO.getEventId(), new FunctionCallback<String>() {
                            @Override
                            public void onFinish(Error error, String data) {
                                if (error != null) {
                                    Log.e(TAG, error.getMessage());
                                }
                            }
                        });
                    }
                }
            }
        });

    }


}
